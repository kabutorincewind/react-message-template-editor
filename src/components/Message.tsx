import * as React from "react";
import * as ReactDOM from "react-dom";

export interface MessageProps { type: string }
export interface MessageState { vars :  Array<string>, values: Array<string>, items: Array<object> }

export class Message extends React.Component<MessageProps, MessageState> {
  constructor(props: MessageProps){
    super(props)
    this.state = {
      vars: ['name', 'surname', 'profession'],
      values: ['Sergey', 'Belous', 'programmer'],
      items: [
        { text: 'You are welcome!'},
        { condition: {
          if: '{name}',
          then: [
            { text: 'Hello {name}.' }
          ],
          else: [
            { text: 'Hello stranger.' }
          ]
        }},
        { condition: {
          if: '{profession}',
          then: [
            { text: 'Your profession is {profession}.' }
          ],
          else: [
            { text: 'Your profession is unknown.' }
          ]
        }},
        { text: 'Have a very safe day.'}
      ]
    };
    this.removeBlock = this.removeBlock.bind(this)
    this.deleteVar = this.deleteVar.bind(this)
    this.addVar = this.addVar.bind(this)
    this.changeVar = this.changeVar.bind(this)
    this.changeValue = this.changeValue.bind(this)
    this.addText = this.addText.bind(this)
    this.addTextSimple = this.addTextSimple.bind(this)
    this.addCondition = this.addCondition.bind(this)
    this.addConditionSimple = this.addConditionSimple.bind(this)
    this.conformStateFromEditable = this.conformStateFromEditable.bind(this)
    this.saveMessage = this.saveMessage.bind(this)
    this.loadMessage = this.loadMessage.bind(this)
  }

  public deleteVar = (event:any) => {
    let datakey = event.target.parentNode.dataset.key
    let vars = [...this.state.vars]
    vars.splice(datakey,1);
    let values = [...this.state.values]
    values.splice(datakey,1);
    this.setState({values: values, vars: vars, items: this.state.items})
  }

  public addVar = (event:any) => {
    let vars = [...this.state.vars]
    vars.push('new var')
    let values = [...this.state.values]
    values.push('new value')
    this.setState({values: values, vars: vars, items: this.state.items})
  }

  public changeVar = (event: any) => {
    let vars = [...this.state.vars]
    vars[event.target.dataset.key] = event.target.value
    this.setState({values: this.state.values, vars: vars, items: this.state.items})
  }

  public changeValue = (event: any) => {
    let values = [...this.state.values]
    values[event.target.dataset.key] = event.target.value
    this.setState({vars: this.state.vars, values: values, items: this.state.items})
  }

  public removeBlock = (event: any) => {
    event.target.parentNode.className = 'deprecated'
    this.conformStateFromEditable()
  }

  public addText = (event: any) => {
    let node = event.target.parentNode
    let pathToInsert: string = node.dataset.refpath
    let realPath = (new RegExp('(.+)\\[(\\d+)\\](\\.condition)?$')).exec(pathToInsert)
    let items = [...this.state.items]
    if(realPath && realPath[2]){
      let start = parseInt(realPath[2])+1
      eval(realPath[1]+'.splice('+start+', 0, {text: "new text"})')
    } else if(realPath) {
      eval(realPath[1]+'.push({text: "new text"})')
    } else {
      eval(pathToInsert+'.push({text: "new text"})')
    }
    this.setState({values: this.state.values, vars: this.state.vars, items: items})
  }

  public addTextSimple = (event: any) => {
    let items = [...this.state.items]
    items.unshift({text: "new text"});
    this.setState({values: this.state.values, vars: this.state.vars, items: items})
  }

  public addCondition = (event: any) => {
    let node = event.target.parentNode
    let pathToInsert: string = node.dataset.refpath
    let realPath = (new RegExp('(.+)\\[(\\d+)\\](\\.condition)?$')).exec(pathToInsert)
    let items = [...this.state.items]
    if(realPath && realPath[2]){
      let start = parseInt(realPath[2])+1
      eval(realPath[1]+'.splice('+start+', 0, {condition: {if: "{var}", then:[{text:""}], else:[{text:""}]}})')
    } else if(realPath) {
      eval(realPath[1]+'.push({condition: {if: "{var}", then:[{text:""}], else:[{text:""}]}})')
    } else {
      eval(pathToInsert+'.push({condition: {if: "{var}", then:[{text:""}], else:[{text:""}]}})')
    }
    this.setState({values: this.state.values, vars: this.state.vars, items: items})
  }

  public addConditionSimple = (event: any) => {
    let items = [...this.state.items]
    items.unshift({condition: {if: "{var}", then:[{text:""}], else:[{text:""}]}});
    this.setState({values: this.state.values, vars: this.state.vars, items: items})
  }

  public refPath: string = 'items'
  public thenRefPath: string = ''
  public elseRefPath: string = ''

  public renderEditableMessage = (el: any, key: number) => {
    const divStyle = {
      margin: '10px 10px 10px 20px'
    }
    if(el.hasOwnProperty('text')){
      let refPath = this.refPath+'['+key+']'
      return (
        <div key={key} data-refpath={refPath} className="text" style={divStyle}>
          text:
          <input value={el.text} onChange={this.conformStateFromEditable}/>
          <button onClick={this.removeBlock}>remove</button>
          <button onClick={this.addText}>+ text</button>
          <button onClick={this.addCondition}>+ condition</button>
        </div>
    ); }
    if(el.hasOwnProperty('condition')){
      var _self = this
      var refPath = this.refPath+'['+key+'].condition'
      var thenRefPath = refPath+'.then'
      var elseRefPath = refPath+'.else'
      var bindThen = () => {_self.refPath = thenRefPath}
      var bindElse = () => {_self.refPath = elseRefPath}
      var clearRefPath = () => {_self.refPath = 'items'}
      return (
        <div key={key} data-refpath={refPath} className="condition" style={divStyle}>
          if: <input value={el.condition.if} onChange={this.conformStateFromEditable}/>
          <button onClick={this.removeBlock}>remove</button>
          <button onClick={this.addText}>+ text</button>
          <button onClick={this.addCondition}>+ condition</button>
          <div className="then" data-refpath={thenRefPath}>
            then:
            <button onClick={this.addText}>+ text</button>
            <button onClick={this.addCondition}>+ condition</button>
            {bindThen()}{el.condition.then.map(this.renderEditableMessage)}
          </div>
          <div className="else" data-refpath={elseRefPath}>
            else:
            <button onClick={this.addText}>+ text</button>
            <button onClick={this.addCondition}>+ condition</button>
            {bindElse()}{el.condition.else.map(this.renderEditableMessage)}
          </div>
          {clearRefPath()}
        </div>
      );
    }
  }
  public interpolateValue = (varName: string) => {
    let regexResult = (new RegExp('\\{(\\w+)\\}')).exec(varName);
    if(regexResult && regexResult[1]){
      var index = this.state.vars.indexOf(regexResult[1])
      return varName.replace((new RegExp('\\{'+this.state.vars[index]+'\\}')), this.state.values[index])
    } else {
      return varName
    }
  }
  public renderMessage = (el: any, key: number) => {
    const divStyle = {
      margin: '10px 10px 10px 20px'
    }
    if(el.hasOwnProperty('text')){ return (<p key={key}> {this.interpolateValue(el.text)} </p>) }
    if(el.hasOwnProperty('condition')){
      let regex = (new RegExp('\{('+this.state.vars.join('|')+')\}'))
      let condition = regex.exec(el.condition.if)
      if ( condition ) {
        return (<div style={divStyle} key={key}> {el.condition.then.map(this.renderMessage)} </div>)
      } else {
        return (<div style={divStyle} key={key}> {el.condition.else.map(this.renderMessage)} </div>)
      }
    }
  };
  public conformStateFromEditable = () => {
    var items:Array<any> = []
    var root:Element = document.querySelector('div.editable-message')
    var childs:any = root.childNodes
    var i:number = childs.length
    function push(root:Element, childs:any, i:number, items:Array<any>){
      var num = 0
      for(var y = 0; y < i; y++) {
        if(childs[y].tagName === 'DIV' ) {
          if(childs[y].className === 'text'){
            num = items.push({text: childs[y].querySelector('input').value}) - 1
          }
          if(childs[y].className === 'condition') {
            num = items.push({condition: {if: childs[y].querySelector('input').value}}) - 1
            items[num].condition.then = []; items[num].condition.else = []
            let thenDiv = childs[y].querySelector('div.then')
            let elseDiv = childs[y].querySelector('div.else')
            push(thenDiv, thenDiv.childNodes, thenDiv.childNodes.length, items[num].condition.then)
            push(elseDiv, elseDiv.childNodes, elseDiv.childNodes.length, items[num].condition.else)
          }
        }
      }
    }
    push(root, childs, i, items)
    this.setState({values: this.state.values, vars: this.state.vars, items: items})
  }

  public saveMessage = () => {
    localStorage.setItem('message', JSON.stringify(this.state))
  }

  public loadMessage = () => {
    this.setState(JSON.parse(localStorage.getItem('message')))
  }

  public render = () => {
    return (
      <div>
        <h3>Printed message</h3>
        <div className="printed-message">
          {this.state.items.map(this.renderMessage)}
        </div>
        <h3>Message vars</h3>
        <div className="message-vars">
          {this.state.vars.map((el: any, key: number) => {
            return (
              <span data-key={key} key={key}>
                <input data-key={key}  value={el} onChange={this.changeVar}/>
                <button onClick={this.deleteVar}> x </button><br/>
                <input data-key={key} value={this.state.values[key]} onChange={this.changeValue}/>
              </span>)
          })}
          <button onClick={this.addVar}>+ variable</button>
        </div>
        <h3>Editable message</h3>
        <div className="editable-message">
          <button onClick={this.addTextSimple}>+ text</button>
          <button onClick={this.addConditionSimple}>+ condition</button>
          {this.state.items.map(this.renderEditableMessage)}
        </div>
        <h3>Store message</h3>
        <div className="store-message">
          <button onClick={this.saveMessage}>Save message</button>
          <button onClick={this.loadMessage}>Load message</button>
        </div>
      </div>
    );
  }
}
